from abc import ABC, abstractmethod
import random
from enum import Enum
import time
import sys


class Price(Enum):

    MAX_HEALTH_POINTS = 100
    INIT_MONEY = 1000
    INIT_HEALTH = 100
    INIT_DAMAGE_POWER = 50
    INIT_DEFENCE_POWER = 20


class Wunderwaffe(ABC):
    def _setup(self):
        self.max_health_points = Price.MAX_HEALTH_POINTS.value
        self.money = Price.INIT_MONEY.value

        self.health = Price.INIT_HEALTH.value
        self.damage_power = Price.INIT_DAMAGE_POWER.value
        self.defence_power = Price.INIT_DEFENCE_POWER.value

    @abstractmethod
    def rise_damage_power(self):
        pass

    @abstractmethod
    def rise_defence_power(self):
        pass

    @abstractmethod
    def __str__(self):
        pass

    @abstractmethod
    def attack(self):
        pass

    @abstractmethod
    def is_alive(self):
        pass


'''def checking_entered_values():
    while True:
        input_value = input('Enter your command: 1 to buy, any other key to exit>>>')
        if int(input_value) == 1:
            break
    return input_value
'''

def _rise_damage_power(instance: Wunderwaffe):
    global possible_rise_damage
    print(f'{instance.name} have {instance.money} dollars')

    if instance.money > instance._price['damage_point']:
        print(f'You can buy only {int(instance.money/instance._price['damage_point'])} damage point(s)')
        command = input('Enter your command: 1 to buy, any other key to exit>>>')

        if command == '1':
            possible_rise_damage = int(input('Enter damage quanity >>>'))

            if possible_rise_damage > int(instance.money/instance._price['damage_point']):
                print('Incorrect quanity')
                return

            instance.damage_power += possible_rise_damage
            instance.money -= possible_rise_damage * instance._price['damage_point']

    else:
        print('You have not enough money to buy')


def _rise_defence_power(instance: Wunderwaffe):
    print(f'{instance.name} have {instance.money} dollars')

    if instance.money > instance._price['defence_point']:
        print(f'You can buy only {int(instance.money/instance._price['defence_point'])} defence point(s)')
        command = input('Enter your command: 1 to buy, any other key to exit>>>')

        if command == '1':
            possible_rise_defence = int(input('Enter defence quanity >>>'))

            if possible_rise_defence > int(instance.money/instance._price['defence_point']):
                print('Incorrect quanity')
                return

            instance.defence_power += possible_rise_defence
            instance.money -= possible_rise_defence * instance._price['defence_point']

    else:
        print('You have not enough money to buy')


def _attack(instance: Wunderwaffe, other: Wunderwaffe):
    if not random.randint(1, 100) < instance.accuracy:
        print(f'{instance.name} missed')
        return

    else:
        if instance.damage_power < other.defence_power:
            pass
        else:
            other.health = other.health - instance.damage_power + other.defence_power

    if other.health < 0:
        other.health = 0

    print(f'{instance.name}: {instance.health} health point(s) >>> {('#'*(int(instance.health/2))).ljust(50,'-')}')
    print(f'{other.name}: {other.health} health point(s) >>> {('#' * (int(other.health / 2))).ljust(50,'_')}')
    print('*'*100)
    print()

    if not other.is_alive():
        print(f'{instance.name} won the battle')
        time.sleep(2)
        sys.exit()


def create_tank_player(name):
    new_tank = Tank(name)
    new_tank.rise_damage_power()
    new_tank.rise_defence_power()

    return new_tank


def create_gavelin_player(name):
    new_gavelin = Tank(name)
    new_gavelin.rise_damage_power()
    new_gavelin.rise_defence_power()

    return new_gavelin


class Tank(Wunderwaffe):

    _price = {
        'damage_point': 25,
        'defence_point': 50,
    }

    def __init__(self, name):
        self._setup()
        self.name = name
        self.damage_power += random.randint(20, 30)
        self.accuracy = random.randint(20, 30)

    def __str__(self):
        return f'{self.name}: health {self.health}pt, damage power {self.damage_power}, defence power {self.defence_power} '

    __repr__ = __str__

    def attack(self, other):
        _attack(self, other)

    def rise_damage_power(self):
        _rise_damage_power(self)

    def rise_defence_power(self):
        _rise_defence_power(self)

    def is_alive(self):
        return self.health > 0


class Gavelin(Wunderwaffe):

    our_instances = {}

    _price = {
        'damage_point': 40,
        'defence_point': 200,
    }

    def __init__(self, name):
        self._setup()
        self.name = name
        self.damage_power -= random.randint(10, 15)
        self.accuracy = random.randint(50, 60)

    def __str__(self):
        return f'{self.name}: health {self.health}pt, damage power {self.damage_power}, defence power {self.defence_power} '

    __repr__ = __str__

    def attack(self, other):
        _attack(self, other)

    @classmethod
    def bulk_creation_gavelin(cls):
        number_new_instances = int(input('Enter number of gavelin production >>>'))

        for i in range(number_new_instances):
            cls.our_instances[f'Gavelin {i + 1}'] = Gavelin(f'Gavelin {i + 1}')

    def rise_damage_power(self):
        _rise_damage_power(self)

    def rise_defence_power(self):
        _rise_defence_power(self)

    def is_alive(self):
        return self.health > 0


class Battle:

    @staticmethod
    def main_battle(player1: Wunderwaffe, player2: Wunderwaffe):
        while True:
            player1.attack(player2)
            player2.attack(player1)


if __name__ == '__main__':

    Gavelin.bulk_creation_gavelin()

    print(Gavelin.our_instances['Gavelin 1'])

    tank1 = create_tank_player('Abrams')
    gavelin1 = create_gavelin_player('Gavelin1')

    players = [tank1, gavelin1]
    random.shuffle(players)

    new_battle = Battle()
    new_battle.main_battle(*players)